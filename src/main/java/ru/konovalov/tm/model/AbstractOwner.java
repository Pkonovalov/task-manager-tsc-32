package ru.konovalov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.konovalov.tm.api.entity.ITWBS;

@Getter
@Setter
@NoArgsConstructor

public abstract class AbstractOwner extends AbstractEntity implements ITWBS {

    private String userId;

}

