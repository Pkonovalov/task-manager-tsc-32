package ru.konovalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.IRepository;
import ru.konovalov.tm.model.AbstractEntity;
import ru.konovalov.tm.model.Task;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final Map<String, E> entities = new LinkedHashMap<>();
    /*    @Nullable
        @Override
        public List<Task> findAll(@NotNull final String userId, @NotNull Comparator<Task> comparator) {
            final List<Task> taskList = new ArrayList<>(Objects.requireNonNull(findAll(userId)));
            taskList.sort(comparator);
            return taskList;
        }

        @Nullable
        @Override
        public List<Task> findAll(@NotNull final String userId) {
            List<Task> list = new ArrayList<>();
            for (Task task : this.tasks) {
                if (userId.equals(task.getUserId())) list.add(task);
            }
            return list;
        }*/
    @NotNull
    protected final List<E> list = new ArrayList<>();

    @NotNull
    @Override
    public List<E> findAll() {
        return new ArrayList<>(entities.values());
    }

    @Override
    public void addAll(@Nullable final Collection<E> collection) {
        if (collection == null) return;
        @NotNull final Map<String, E> newEntities = collection.stream()
                .collect(Collectors.toMap(E::getId, Function.identity(), (o1, o2) -> o1, LinkedHashMap::new));
        entities.putAll(newEntities);
    }

    @Override
    public void add(@Nullable final E entity) {
        if (entity == null) return;
        list.add(entity);
    }

    @Override
    public void remove(@Nullable final E entity) {
        list.remove(entity);
    }

    @Override
    public int size() {
        return list.size();
    }

    public E removeById(@NotNull final String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        list.remove(entity);
        return null;
    }

    public E findById(@NotNull final String id) {
        for (E entity : list) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public void clear() {
        entities.clear();
    }

}



