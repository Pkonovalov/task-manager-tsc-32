package ru.konovalov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.api.service.IPropertyService;
import ru.konovalov.tm.bootstrap.Bootstrap;
import ru.konovalov.tm.command.domain.BackupLoadCommand;
import ru.konovalov.tm.command.domain.BackupSaveCommand;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;
    private final int interval;

    public Backup(@NotNull final Bootstrap bootstrap, @NotNull final IPropertyService propertyService) {
        this.bootstrap = bootstrap;
        this.interval = propertyService.getBackupInterval();
    }

    public void init() {
        load();
        start();
    }

    public void start() {
        es.scheduleWithFixedDelay(this, 0, interval, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void load() {
        bootstrap.parseCommand(BackupLoadCommand.NAME);
    }

    public void run() {
        bootstrap.parseCommand(BackupSaveCommand.NAME);
    }

}
