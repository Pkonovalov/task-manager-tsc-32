package ru.konovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.model.Task;
import ru.konovalov.tm.model.User;
import ru.konovalov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskByProjectIdListCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-list-by-project-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list by project id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        final List<Task> taskList = serviceLocator.getProjectTaskService().findALLTaskByProjectId(userId, TerminalUtil.nextLine());
        int index = 1;
        for (final Task task : taskList) {
            System.out.println(index + ".\t" + task);
            index++;
        }
    }

}
