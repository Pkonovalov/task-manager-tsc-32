package ru.konovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.exeption.empty.EmptyNameException;
import ru.konovalov.tm.exeption.system.IndexIncorrectException;
import ru.konovalov.tm.util.TerminalUtil;

import static ru.konovalov.tm.util.ValidationUtil.checkIndex;
import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public final class TaskByIndexUpdateCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER INDEX:]");
        final int index = TerminalUtil.nextNumber() - 1;
        if (!checkIndex(index, serviceLocator.getTaskService().size(userId))) throw new IndexIncorrectException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        serviceLocator.getTaskService().updateTaskByIndex(userId, index, name, TerminalUtil.nextLine());
    }

}
