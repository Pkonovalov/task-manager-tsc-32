package ru.konovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.util.TerminalUtil;

public class ProjectByIndexStartCommand extends AbstractProjectCommand {
    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-start-by-index";
    }

    @Override
    public @NotNull String description() {
        return "Start project by index";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[START PROJECT]");
        System.out.println("ENTER INDEX:");
        serviceLocator.getProjectService().startProjectByIndex(userId, TerminalUtil.nextNumber() - 1);
    }

}
