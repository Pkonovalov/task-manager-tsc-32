package ru.konovalov.tm.command.project;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.exeption.entity.ProjectNotFoundException;
import ru.konovalov.tm.model.Project;
import ru.konovalov.tm.model.User;
import ru.konovalov.tm.util.TerminalUtil;

public class ProjectByIdStartCommand extends AbstractProjectCommand {
    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-start-by-id";
    }

    @Override
    public @NotNull String description() {
        return "Start project by id";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("[START PROJECT]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().startProjectById(user.getId(), id);
        if (project == null) throw new ProjectNotFoundException();
    }

}

