package ru.konovalov.tm.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public final class DataXmlLoadJaxBCommand extends AbstractDomainCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return "Load xml data from file";
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-jaxb-load";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML JAXB LOAD]");
        @NotNull final File file = new File(FILE_JAXB_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);

        setDomain(domain);
    }

}